﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    public GameObject Menu;
    public GameObject MenuList;
    public GameObject Help;
    public GameObject Unduh;
    public string Url;
    public GameObject CandiPrambanan;
    public GameObject CandiBorobudur;
    public GameObject CandiGedongSongo;
    public GameObject CandiPlaosan;
    public GameObject CandiRatuBoko;
    public GameObject CandiSambisari;
    public GameObject CandiAbang;
    public GameObject CandiBanyunibo;
    public GameObject CandiIjo;
    // Start is called before the first frame update
    public void Start()
    {
        Menu.SetActive(true);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);

    }
   public void MenuListClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(true);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void PlayButtonClicked()
    {
        Application.LoadLevel("scangambar");
    }

    public void HelpClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(true);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void Open()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(true);
        Application.OpenURL(Url);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void Quit_Clicked()
    {
        Application.Quit();
    }
    public void BackButtonClicked()
    {
        Menu.SetActive(true);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiPrambananClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(true);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiBorobudurClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(true);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiGedongSongoClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(true);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiPlaosanClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(true);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiRatuBokoClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(true);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiSambisariClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(true);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiAbangClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(true);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(false);
    }
    public void CandiBanyuniboClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(true);
        CandiIjo.SetActive(false);
    }
    public void CandiIjoClicked()
    {
        Menu.SetActive(false);
        MenuList.SetActive(false);
        Help.SetActive(false);
        Unduh.SetActive(false);
        CandiPrambanan.SetActive(false);
        CandiBorobudur.SetActive(false);
        CandiGedongSongo.SetActive(false);
        CandiPlaosan.SetActive(false);
        CandiRatuBoko.SetActive(false);
        CandiSambisari.SetActive(false);
        CandiAbang.SetActive(false);
        CandiBanyunibo.SetActive(false);
        CandiIjo.SetActive(true);
    }
}
